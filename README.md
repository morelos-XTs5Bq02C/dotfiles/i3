Custom files for i3

These files are modified to taste.
Read first before deploying!

To deploy, open a terminal in the same directory as this file and then:

~~~shell
./deploy.sh
~~~

This will copy the i3 config. 

The config.toml file belongs to i3bar-rs
To deploy, simply do:

~~~shell
./deploy_status.sh
~~~

Now we need to add it to our greeter.
At the time of writing this README, lemurs is the chosen greeter.
As such, to add i3, we need to do the following:

~~~shell
sudo ./deploy_lemur.sh
~~~

This command requires a sudoer as we're copying to a folder inside /etc/

If you need to add something to the autostart, you can do it by editing
i3 (the lemur file) or by editing config (trough exec or exec-once).

Files will be sent to their respective destinations as specified 
by the Arch Wiki.

If you need them elsewhere, modify deploy.sh to your needs.
